<?php

/**
 * @file
 * Aviary
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */
use vierbergenlars\SemVer\version;
use vierbergenlars\SemVer\expression;
use vierbergenlars\SemVer\SemVerException;

define('AVIARY_NOT_INSTALLED', 1);
define('AVIARY_VERSION_INCOMPATIBLE', 2);
define('AVIARY_VERSION_PARSE_ERROR', 3);
define('AVIARY_EXPRESSION_PARSE_ERROR', 4);

/**
 * Default Bower registry URL.
 */
define('AVIARY_BOWER_REGISTRY_URL', 'https://bower.herokuapp.com');

/**
 * Implements hook_menu().
 */
function aviary_menu() {
  $items = array();
  $path = drupal_get_path('module', 'aviary');

  $items['admin/config/development/aviary'] = array(
    'title' => 'Aviary',
    'description' => 'Configuration for Aviary.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_settings'),
    'access arguments' => array('administer aviary'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'aviary.admin.inc',
    'file path' => $path,
  );

  $items['admin/config/development/aviary/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_settings'),
    'access arguments' => array('administer aviary'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'aviary.admin.inc',
    'file path' => $path,
  );

  $items['admin/config/development/aviary/hooks'] = array(
    'title' => 'Hooks',
    'description' => 'Export Bower dependencies to Aviary file.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_hooks'),
    'access arguments' => array('administer aviary'),
    'file' => 'aviary.admin.hooks.inc',
    'file path' => $path,
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/config/development/aviary/makefile'] = array(
    'title' => 'makefile',
    'description' => 'makefile configuration for Aviary.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_makefile_settings'),
    'access arguments' => array('administer aviary'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'aviary.admin.makefile.inc',
    'file path' => $path,
  );

  $items['admin/config/development/aviary/makefile/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_makefile_settings'),
    'access arguments' => array('administer aviary'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'aviary.admin.makefile.inc',
    'file path' => $path,
  );

  $items['admin/config/development/aviary/makefile/generator'] = array(
    'title' => 'makefile Generation',
    'description' => 'makefile generation for Aviary.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aviary_admin_makefile_generation'),
    'access arguments' => array('administer aviary'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'aviary.admin.makefile.inc',
    'file path' => $path,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function aviary_permission() {
  return array(
    'administer aviary' => array(
      'title' => t('Administer Aviary'),
    ),
  );
}

/**
 * Implements hook_xautoload().
 */
function aviary_xautoload($adapter) {
  $adapter->addClassmapSources(array('includes/php-semver/src'));
}

/**
 * Implements hook_init().
 */
function aviary_init() {
  // Load hook_libraries_info() implementation of modules.
  foreach (array_keys(aviary_list_modules()) as $name) {
    $path = aviary_get_export_path('module', $name);
    if (file_exists($path)) {
      require_once($path);
    }
  }

  // Load hook_libraries_info() implementation of themes.
  foreach (array_keys(aviary_list_themes()) as $name) {
    $path = aviary_get_export_path('theme', $name);
    if (file_exists($path)) {
      require_once($path);
    }
  }
}

/**
 * Retrieves a list of modules with manifest files.
 *
 * @return array
 */
function aviary_list_modules() {
  $modules = array();

  foreach (module_list() as $module) {
    $path = drupal_get_path('module', $module);

    if (file_exists($path . '/bower.json')) {
      $modules[$module] = $path;
    }
  }

  return $modules;
}

/**
 * Retrieves a list of themes with manifest files.
 *
 * @return array
 */
function aviary_list_themes() {
  $themes = array();

  foreach (list_themes() as $theme) {
    $path = drupal_get_path('theme', $theme->name);

    if (file_exists($path . '/bower.json')) {
      $themes[$theme->name] = $path;
    }
  }

  return $themes;
}

/**
 * Retrieves a list of packages in a given directory.
 *
 * @param string $directory
 * @return type
 */
function aviary_list_packages($directory) {
  $cache = &drupal_static(__FUNCTION__);

  if (!isset($cache)) {
    $cache = array();
  }

  if (isset($cache[$directory])) {
    return $cache[$directory];
  }

  $cache[$directory] = array();
  $files = file_scan_directory($directory, '/bower\.json/');

  foreach ($files as $path => $file) {
    if ($manifest = aviary_parse_manifest($path)) {
      // Parse meta manifest created by Bower when a package is installed.
      $package_directory = dirname(drupal_realpath($path));
      if ($meta_manifest = aviary_parse_manifest($package_directory . '/.bower.json')) {
        foreach ($meta_manifest as $key => $value) {
          if (!isset($manifest[$key])) {
            $manifest[$key] = $value;
          }
        }
      }

      $name = $manifest['name'];
      $cache[$directory][$name] = $manifest;
    }
  }

  return $cache[$directory];
}

/**
 * Retrieves info about a package from a manifest file.
 *
 * @param string $name Name of package in libraries path.
 * @param string $path Path to package. Path is auto-detected if not given.
 * @return boolean|array
 */
function aviary_get_info($name, $path = NULL) {
  $cache = &drupal_static(__FUNCTION__);

  if (!isset($cache)) {
    $cache = array();
  }

  if (isset($cache[$name])) {
    return $cache[$name];
  }

  if ($path === NULL) {
    $path = libraries_get_path($name);
  }

  if (!$path) {
    $cache[$name] = FALSE;
    return $cache[$name];
  }

  $manifest = aviary_parse_manifest($path . '/bower.json');

  if ($manifest) {
    // Parse meta manifest created by Bower when a package is installed.
    if ($meta_manifest = aviary_parse_manifest($path . '/.bower.json')) {
      foreach ($meta_manifest as $key => $value) {
        if (!isset($manifest[$key])) {
          $manifest[$key] = $value;
        }
      }
    }

    $manifest['path'] = $path;
  }

  $cache[$name] = $manifest;

  return $cache[$name];
}

/**
 * Retrieves the version of a package from a manifest file.
 *
 * @param string $name
 * @return boolean|string
 */
function aviary_get_version($name) {
  $info = aviary_get_info($name);

  if (!$info) {
    return FALSE;
  }

  return $info['version'];
}

/**
 * Parses a Bower manifest file.
 *
 * @param string $manifest_path
 * @return boolean|array
 */
function aviary_parse_manifest($manifest_path) {
  $cache = &drupal_static(__FUNCTION__);

  if (!isset($cache)) {
    $cache = array();
  }

  if (isset($cache[$manifest_path])) {
    return $cache[$manifest_path];
  }

  if (!file_exists($manifest_path)) {
    $cache[$manifest_path] = FALSE;
    return $cache[$manifest_path];
  }

  $data = file_get_contents($manifest_path);

  if (empty($data)) {
    $cache[$manifest_path] = FALSE;
    return $cache[$manifest_path];
  }

  $json = drupal_json_decode($data);

  if (!is_array($json)) {
    $cache[$manifest_path] = FALSE;
    return $cache[$manifest_path];
  }

  $directory = dirname(drupal_realpath($manifest_path));
  $parts = explode('/', $directory);
  $name = end($parts);

  $defaults = array(
    'name' => $name,
    'version' => NULL,
    'homepage' => NULL,
    'dependencies' => array(),
    'devDependencies' => array(),
  );
  foreach ($defaults as $key => $value) {
    if (!isset($json[$key])) {
      $json[$key] = $value;
    }
  }

  if (!is_array($json['dependencies'])) {
    $json['dependencies'] = array();
  }
  if (!is_array($json['devDependencies'])) {
    $json['devDependencies'] = array();
  }

  $cache[$manifest_path] = $json;

  return $cache[$manifest_path];
}

/**
 * Retrieves a list of dependencies of a given package.
 *
 * @param string $name
 * @param string $path
 * @return boolean|array Returns an array of dependencies, or FALSE if the given package is not installed.
 */
function aviary_get_dependencies($name, $path = NULL) {
  $info = aviary_get_info($name, $path);

  if (empty($info)) {
    return FALSE;
  }

  $packages = array();

  foreach ($info['dependencies'] as $name => $version) {
    $dependencies = aviary_get_dependencies($name);

    if (is_array($dependencies)) {
      $packages = array_merge($packages, $dependencies);
    }

    $packages[$name] = $version;
  }

  return $packages;
}

/**
 * Checks if all dependencies of a module or theme are installed.
 *
 * @param string $type module, theme
 * @param string $name Name of module or theme.
 * @return boolean|array Returns TRUE if all dependencies are installed, or an array of warnings if not.
 */
function aviary_check_dependencies($type, $name) {
  $path = drupal_get_path($type, $name);

  if (empty($path)) {
    watchdog('aviary', '%name %type not found', array('%type' => $type, '%name' => $name), WATCHDOG_ERROR);
    return FALSE;
  }

  $manifest_path = $path . '/bower.json';

  if (!file_exists($manifest_path)) {
    return TRUE;
  }

  $manifest = aviary_parse_manifest($manifest_path);

  if (empty($manifest)) {
    watchdog('aviary', 'Cannot parse manifest: %path', array('%path' => $manifest_path), WATCHDOG_ERROR);
    return FALSE;
  }

  return _aviary_check_dependencies($manifest['dependencies']);
}

/**
 * Checks if the given dependencies are installed.
 *
 * @param array $packages
 * @return boolean|array
 * @see aviary_check_dependencies()
 */
function _aviary_check_dependencies(array $packages) {
  $result = array();
  $installed = aviary_list_packages('sites/all/libraries');

  foreach ($packages as $package => $version) {
    $index = strrpos($version, '#');

    if ($index !== FALSE) {
      $version = substr($version, $index + 1);
    }

    if (!isset($installed[$package])) {
      $result[$package] = array(
        'version' => $version,
        'code' => AVIARY_NOT_INSTALLED,
        'message' => t('Package is not installed: !package.', array('!package' => $package)),
      );
      continue;
    }

    if (!empty($installed[$package]['dependencies'])) {
      $dependencies = _aviary_check_dependencies($installed[$package]['dependencies']);

      if (is_array($dependencies)) {
        $result = array_merge($result, $dependencies);
      }
    }

    try {
      $semver = new version($installed[$package]['version']);
    }
    catch (Exception $exception) {
      $result[$package] = array(
        'version' => $version,
        'code' => AVIARY_VERSION_PARSE_ERROR,
        'message' => t('Cannot parse version of installed package: !name#!version', array('!name' => $package, '!version' => $installed[$package]['version'])),
      );
      watchdog('aviary', 'Cannot parse version of installed package: !name#!version', array('!name' => $package, '!version' => $installed[$package]['version']), WATCHDOG_ERROR);
      continue;
    }

    try {
      $expression = new expression($version);
    }
    catch (Exception $exception) {
      $result[$package] = array(
        'version' => $version,
        'code' => AVIARY_EXPRESSION_PARSE_ERROR,
        'message' => t('Cannot parse version of package: !name#!version', array('!name' => $package, '!version' => $version)),
      );
      continue;
    }

    if (!$semver->satisfies($expression)) {
      $result[$package] = array(
        'version' => $version,
        'code' => AVIARY_VERSION_INCOMPATIBLE,
        'message' => t('Installed package is incompatible: !name#!version (requires !required_version)', array('!name' => $package, '!version' => $installed[$package]['version'], '!required_version' => $version)),
      );
    }
  }

  if (empty($result)) {
    return TRUE;
  }

  return $result;
}

/**
 * Retrieves a list of libraries of a module or theme.
 *
 * @param string $type module, theme
 * @param string $name Name of module or theme.
 * @return array
 */
function aviary_get_libraries($type, $name) {
  $cache = &drupal_static(__FUNCTION__);

  if (!isset($cache)) {
    $cache = array();
  }

  if (isset($cache[$name])) {
    return $cache[$name];
  }

  $data = array();

  if ($type === 'module') {
    if (in_array($name, module_implements('aviary_data'))) {
      $fn = $name . '_aviary_data';
      $data = $fn();
    }
  }
  elseif ($type === 'theme') {
    require_once(drupal_get_path('theme', $name) . '/template.php');
    $fn = $name . '_aviary_data';

    if (function_exists($fn)) {
      $data = $fn();
    }
  }

  $libraries = isset($data['libraries']) ? $data['libraries'] : array();
  $exclude = isset($data['exclude']) ? $data['exclude'] : array();
  $rename = isset($data['rename']) ? $data['rename'] : array();

  $cache[$name] = _aviary_build_libraries($type, $name, $libraries, $exclude, $rename);

  return $cache[$name];
}

/**
 * Builds data for hook_libraries_info() implementation.
 *
 * @param string $type module, theme
 * @param string $name Name of module or theme.
 * @param array $libraries An associative array whose keys are internal names of libraries and whose values are describing each library. Each key is the directory name below the 'libraries' directory, in which the library may be found. See http://www.drupalcontrib.org/api/drupal/contributions!libraries!libraries.api.php/function/hook_libraries_info/7
 * @param array $exclude Array of library names to exclude. This is used to ignore dependency errors for libraries not registered with hook_libraries_info(), e.g. jquery and ui (jquery-ui).
 * @param array $rename Array of search-replace pairs. Some packages are available under multiple names, e.g. "angularjs" and "angular". Renaming is needed to ensure only one version is loaded.
 * @return array
 */
function _aviary_build_libraries($type, $name, $libraries, array $exclude = array(), array $rename = array()) {
  $build = array();

  foreach ($libraries as $library_name => $library_options) {
    if (in_array($library_name, $exclude)) {
      continue;
    }

    if (isset($rename[$library_name])) {
      $library_name = $rename[$library_name];
    }

    if (!isset($library_options['library path'])) {
      $library_options['library path'] = NULL;
    }

    $info = aviary_get_info($library_name, $library_options['library path']);

    if (!$info) {
      continue;
    }

    $build[$library_name]['version'] = $info['version'];

    if (!empty($library_options['library path'])) {
      $build[$library_name]['library path'] = $library_options['library path'];
    }

    if (!empty($library_options['path'])) {
      $build[$library_name]['path'] = $library_options['path'];
    }

    foreach (array('css', 'js') as $key) {
      if (isset($library_options['files'][$key])) {
        foreach ($library_options['files'][$key] as $file => $options) {
          if (!is_array($options)) {
            $file = $options;
            $options = array();
          }

          $build[$library_name]['files'][$key][$file] = $options;
        }
      }
    }

    if (isset($library_options['files']['php'])) {
      $build[$library_name]['files']['php'] = $library_options['files']['php'];
    }

    foreach (array_keys($info['dependencies']) as $dependency) {
      if (in_array($dependency, $exclude)) {
        continue;
      }

      if (isset($rename[$dependency])) {
        $dependency = $rename[$dependency];
      }

      $build[$library_name]['dependencies'][] = $dependency;
    }
  }

  return $build;
}

/**
 * Saves a hook_libraries_info() implementation to a file.
 *
 * @param string $type module, theme
 * @param string $name Name of module or theme.
 * @param boolean|string $write_to_file Returns output if set to FALSE, or writes to default path if set to TRUE, or writes to given path if a string.
 * @return boolean|string
 */
function aviary_export_libraries($type, $name, $write_to_file = FALSE) {
  $libraries = aviary_get_libraries($type, $name);
  $lf = "\n";

  $output = '<?php

/**
 * @file
 * Libraries registered by the ' . $name . ' ' . $type . '. File generated by aviary_export_libraries().
 */

/**
 * Implements hook_libraries_info().
 */' . $lf;
  $output .= 'function ' . $name . '_libraries_info () {' . $lf;
  $output .= 'return ' . var_export($libraries, TRUE) . ';' . $lf;
  $output .= '}' . $lf;

  if ($write_to_file === FALSE) {
    return $output;
  }

  $path = '';

  if ($write_to_file === TRUE) {
    $path = aviary_get_export_path($type, $name);
  }
  else {
    $path = $write_to_file;
  }

  if (empty($path)) {
    drupal_set_message(t('Failed to write export file because path is empty.'), 'error');
    return FALSE;
  }

  $directory = drupal_dirname($path);
  file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  if (!file_exists($path) || is_writable($path)) {
    if (file_put_contents($path, $output) === FALSE) {
      drupal_set_message(t('Failed to write to <code>!file</code>. Please check the write permissions.', array('!file' => pathinfo($path, PATHINFO_BASENAME))), 'error');
      return FALSE;
    }
  }
  else {
    drupal_set_message(t('<code>!file</code> not writable. Please check the write permissions.', array('!file' => pathinfo($path, PATHINFO_BASENAME))), 'error');
    return FALSE;
  }

  return TRUE;
}

/**
 * Returns the path to the file with the exported hook_libraries_info() implementation.
 *
 * @param string $type module, theme
 * @param string $name Name of module or theme.
 * @return boolean|string
 */
function aviary_get_export_path($type, $name) {
  $file = $name . '.aviary.inc';

  if ($type === 'module') {
    if (in_array($name, module_implements('aviary_api'))) {
      $fn = $name . '_aviary_api';
      $api = $fn();

      if (!empty($api['path'])) {
        return $api['path'] . '/' . $file;
      }
    }
  }
  elseif ($type === 'theme') {
    require_once(drupal_get_path('theme', $name) . '/template.php');
    $fn = $name . '_aviary_api';

    if (function_exists($fn)) {
      $api = $fn();

      if (!empty($api['path'])) {
        return $api['path'] . '/' . $file;
      }
    }
  }

  return FALSE;
}

/**
 * Performs an HTTP request. Similar to drupal_http_request() but uses cURL
 * instead of socket connections, if possible.
 *
 * @param string $url
 * @param array $options
 * @return stdClass
 */
function aviary_http_request($url, array $options = array()) {
  if (function_exists('curl_exec')) {
    // Merge the default options.
    $options += array(
      'headers' => array(),
      'method' => 'GET',
      'data' => NULL,
      'max_redirects' => 3,
      'timeout' => 30.0,
      'context' => NULL,
    );

    // Merge the default headers.
    $options['headers'] += array(
      'User-Agent' => 'Drupal (+http://drupal.org/)',
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);
    curl_setopt($ch, CURLOPT_USERAGENT, $options['headers']['User-Agent']);
    curl_setopt($ch, CURLOPT_MAXREDIRS, $options['max_redirects']);
    curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    $data = curl_exec($ch);
    curl_close($ch);

    return (object) array(
          'error' => FALSE,
          'data' => $data,
    );
  }

  return drupal_http_request($url, $options);
}

/**
 * Generates makefile library entries for the dependencies of the currently installed modules and themes.
 *
 * @param boolean|string $write_to_file Path to makefile, or FALSE if output should be returned.
 * @return boolean|string Returns TRUE if the makefile could be written, FALSE if write failed, or makefile entries if $write_to_file is FALSE.
 */
function aviary_generate_makefile($write_to_file = FALSE) {
  $packages = array();

  foreach (aviary_list_modules() as $name => $path) {
    $dependencies = aviary_get_dependencies($name, $path);

    if (is_array($dependencies)) {
      $packages = array_merge($packages, $dependencies);
    }
  }

  foreach (aviary_list_themes() as $name => $path) {
    $dependencies = aviary_get_dependencies($name, $path);

    if (is_array($dependencies)) {
      $packages = array_merge($packages, $dependencies);
    }
  }

  ksort($packages);
  $lf = "\n";
  $output = '';

  if ($write_to_file) {
    if (!is_string($write_to_file)) {
      drupal_set_message(t('Path to makefile is not a string.'), 'error');
      return FALSE;
    }

    $file_path = $write_to_file;

    $entries = array();

    foreach ($packages as $name => $version) {
      $entries[$name] = _aviary_build_makefile_entry_lines($name, $version);
    }

    $names = array_keys($entries);
    $lines = array();

    if (file_exists($file_path)) {
      $output = file_get_contents($file_path);

      // Normalize newline.
      $output = str_replace(array("\r\n", "\r"), "\n", $output);

      $lines = explode("\n", $output);
    }

    // Line number of last line with a "libraries[]" entry.
    $last = -1;

    // Remove old entries.
    foreach ($lines as $i => $line) {
      if (preg_match('/^[\s;]*libraries\[([^\]]+)\]/i', $line, $matches)) {
        $last = $i;

        if (in_array($matches[1], $names)) {
          unset($lines[$i]);
          $last--;
        }
      }
    }

    $new_lines = array();

    foreach ($entries as $name => $entry) {
      $entry[0] = "\n" . $entry[0];
      $new_lines = array_merge($new_lines, $entry);
    }

    if ($last > -1) {
      // Insert new entries between existing entries.
      $lines = array_merge(array_slice($lines, 0, $last + 1), $new_lines, array_slice($lines, $last + 1));
    }
    else {
      // Append new entries.
      $lines = array_merge($lines, $new_lines);
    }

    $output = implode("\n", $lines);

    if (!file_exists($file_path) || is_writable($file_path)) {
      if (file_put_contents($file_path, $output) === FALSE) {
        drupal_set_message(t('Failed to write to <code>!file</code>. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
        return FALSE;
      }
    }
    else {
      drupal_set_message(t('<code>!file</code> not writable. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
      return FALSE;
    }

    return TRUE;
  }

  foreach ($packages as $name => $version) {
    $output .= implode($lf, _aviary_build_makefile_entry_lines($name, $version)) . $lf . $lf;
  }

  return rtrim($output);
}

/**
 * Generates a makefile entry for a package.
 *
 * @param string $name Package name.
 * @return boolean|array Returns an array for a makefile entry, or FALSE if package cannot be resolved.
 */
function _aviary_build_makefile_entry_data($name) {
  $info = aviary_get_info($name);

  if (empty($info)) {
    return FALSE;
  }

  $parse = array();
  $source = isset($info['_source']) ? $info['_source'] : NULL;

  // Git case: git git+ssh, git+http, git+https
  //           .git at the end (probably ssh shorthand)
  //           git@ at the start
  if (preg_match('/^git(\+(ssh|https?))?:\/\//i', $source) || preg_match('/\.git\/?$/i', $source) || preg_match('/^git@/i', $source)) {
    $type = $info['_resolution']['type'];
    $branch = isset($info['_resolution']['branch']) ? $info['_resolution']['branch'] : 'master';
    $tag = isset($info['_resolution']['tag']) ? $info['_resolution']['tag'] : NULL;
    $commit = isset($info['_resolution']['commit']) ? $info['_resolution']['commit'] : NULL;
    $public = FALSE;
    $github_user = NULL;
    $github_repo = NULL;

    // Check if it's a GitHub repository.
    if (preg_match('/(?:@|:\/\/)github.com[:\/]([^\/\s]+?)\/([^\/\s]+?)(?:\.git)?\/?$/i', $source, $matches)) {
      $github_user = $matches[1];
      $github_repo = $matches[2];

      // Ensure trailing for all protocols.
      if (strcasecmp(substr($source, -4), '.git') !== 0) {
        $source .= '.git';
      }

      // Check if the repository is public.
      if (strcasecmp(substr($source, 0, 6), 'git://') === 0) {
        $public = TRUE;
      }
    }

    // Public GitHub.
    if ($github_user && $github_repo && $public && $tag) {
      $parse['directory'] = $name;
      $parse['download']['type'] = 'get';
      $parse['download']['url'] = 'https://github.com/' . $github_user . '/' . $github_repo . '/archive/' . $tag . '.tar.gz';
    }
    // Private GitHub or another Git.
    else {
      if ($type === 'version') {
        if ($commit) {
          $parse['download']['type'] = 'git';
          $parse['download']['url'] = $source;
          $parse['download']['branch'] = $branch;
          $parse['download']['revision'] = $commit;
        }
      }
      elseif ($type === 'tag') {
        if ($tag) {
          $parse['download']['type'] = 'git';
          $parse['download']['url'] = $source;
          $parse['download']['tag'] = $tag;
        }
      }
      elseif ($type === 'branch') {
        $parse['download']['type'] = 'git';
        $parse['download']['url'] = $source;
        $parse['download']['branch'] = $branch;
      }
    }
  }
  // SVN case: svn, svn+ssh, svn+http, svn+https, svn+file
  elseif (preg_match('/^svn(\+(ssh|https?|file))?:\/\//i', $source)) {
    $parse['download']['type'] = 'svn';
    $parse['download']['url'] = $source;
  }
  // URL case
  elseif (preg_match('/^https?:\/\//i', $source)) {
    $parse['directory'] = $name;
    $parse['download']['type'] = 'get';
    $parse['download']['url'] = $source;
  }

  if (empty($parse)) {
    return FALSE;
  }

  $parse['type'] = 'library';

  return $parse;
}

/**
 * Generates the strings for a makefile entry for a package.
 *
 * @param string $name
 * @param string $version
 * @return array
 */
function _aviary_build_makefile_entry_lines($name, $version) {
  $lines = array();
  $entry = _aviary_build_makefile_entry_data($name);

  if (empty($entry)) {
    drupal_set_message(t('Could not generate a makefile entry for "!package".', array('!package' => $name)), 'error');

    $info = aviary_get_info($name);
    $homepage = '';

    if (!empty($info)) {
      if (!empty($info['homepage'])) {
        $homepage = $info['homepage'];
      }
    }

    $lines[] = '; libraries[' . $name . '][type] = "library" ' . $name . ' (' . $version . ')';
    $lines[] = '; libraries[' . $name . '][download][type] = ""';
    $lines[] = '; libraries[' . $name . '][download][url] = "" ' . $homepage;
  }
  else {
    foreach ($entry as $gkey => $gval) {
      if (is_array($gval)) {
        foreach ($gval as $key => $val) {
          $lines[] = 'libraries[' . $name . '][' . $gkey . '][' . $key . '] = "' . $val . '"';
        }
      }
      else {
        $lines[] = 'libraries[' . $name . '][' . $gkey . '] = "' . $gval . '"';
      }
    }
  }

  return $lines;
}
