# aviary

Bower integration for modules and themes.

## Usage

### Check the dependencies of a module or theme

```
aviary_check_dependencies('module', 'MODULE_NAME');
```

Returns `TRUE` if all dependencies are installed in `sites/all/libraries` and have compatible versions, else an array of warnings.

Format of warning:

```
'PACKAGE_NAME' => array(
  'version' => 'PACKAGE_VERSION',
  'code' => AVIARY_* constant
  'message' =>
)
```

## Drush

Command                       | Description
----------------------------- | ------------------------------------------------
drush aviary-install          | Install Bower dependencies defined in modules and themes
drush aviary-update           | Update Bower dependencies defined in modules and themes
drush aviary-export-hooks     | Export Bower dependencies defined in modules and themes to hooks file
drush aviary-make-generate    | Generate a makefile from the Bower dependencies defined in modules and themes

Command                       | Alias
----------------------------- | ------------------------------------------------
aviary-make-generate          | aviary-generate-makefile

## Configuration

Set the path where Bower components should be installed in the `.bowerrc` file in the module or theme directory.

```
{
  "directory": "../../libraries"
}
```

Note: This is only required if executing the `bower` command in a module or theme directory. If using Drush, then this directory is set automatically.

## Codes

Constant                      | Description
----------------------------- | ------------------------------------------------
AVIARY_NOT_INSTALLED          | Package is not installed
AVIARY_VERSION_INCOMPATIBLE   | Package is installed but version is incompatible
AVIARY_VERSION_PARSE_ERROR    | Cannot parse version of installed package
AVIARY_EXPRESSION_PARSE_ERROR | Cannot parse version of dependency

## Credits

* [php-semver](https://github.com/vierbergenlars/php-semver) (Lars Vierbergen, MIT license)