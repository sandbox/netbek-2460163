<?php

/**
 * @file
 * Drush integration for the Aviary module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Implement hook_drush_help().
 */
function aviary_drush_help($section) {
  switch ($section) {
    case 'drush:aviary-install':
      return dt('Install Bower dependencies defined in modules and themes.');
    case 'drush:aviary-update':
      return dt('Update Bower dependencies defined in modules and themes.');
    case 'drush:aviary-export-hooks':
      return dt('Export Bower dependencies defined in modules and themes to hooks file.');
    case 'drush:aviary-make-generate':
      return dt('Generate a makefile from the Bower dependencies defined in modules and themes.');
  }
}

/**
 * Implement hook_drush_command().
 */
function aviary_drush_command() {
  $items = array();

  $items['aviary-install'] = array(
    'description' => 'Install Bower dependencies defined in modules and themes.',
  );

  $items['aviary-update'] = array(
    'description' => 'Update Bower dependencies defined in modules and themes.',
  );

  $items['aviary-export-hooks'] = array(
    'description' => 'Export Bower dependencies defined in modules and themes to hooks file.',
  );

  $items['aviary-make-generate'] = array(
    'description' => 'Generate a makefile from the Bower dependencies defined in modules and themes.',
    'aliases' => array('aviary-generate-makefile'),
  );

  return $items;
}

/**
 * Callback function for the aviary-install command.
 */
function drush_aviary_install() {
  $operations = array();

  $modules = aviary_list_modules();
  foreach ($modules as $name => $path) {
    $operations[$name] = array(
      'cwd' => drupal_realpath($path),
      'lib' => '../../../all/libraries',
    );
  }

  $themes = aviary_list_themes();
  foreach ($themes as $name => $path) {
    $operations[$name] = array(
      'cwd' => drupal_realpath($path),
      'lib' => '../../../all/libraries',
    );
  }

  if (empty($operations)) {
    drush_log(dt('No Bower packages to install.'), 'ok');
  }
  else {
    foreach ($operations as $name => $paths) {
      drush_log(dt('Installing Bower dependencies of !name.', array('!name' => $name)), 'notice');

      $command = 'bower install --config.directory=%s --allow-root';
      if (drush_shell_cd_and_exec($paths['cwd'], $command, $paths['lib'])) {
        drush_log(dt('Successfully installed Bower dependencies of !name.', array('!name' => $name)), 'ok');
      }
      else {
        drush_log(dt('Failed to install Bower dependencies of !name.', array('!name' => $name)), 'error');
      }
    }
  }
}

/**
 * Callback function for the aviary-update command.
 */
function drush_aviary_update() {
  $operations = array();

  $modules = aviary_list_modules();
  foreach ($modules as $name => $path) {
    $operations[$name] = array(
      'cwd' => drupal_realpath($path),
      'lib' => '../../../all/libraries',
    );
  }

  $themes = aviary_list_themes();
  foreach ($themes as $name => $path) {
    $operations[$name] = array(
      'cwd' => drupal_realpath($path),
      'lib' => '../../../all/libraries',
    );
  }

  if (empty($operations)) {
    drush_log(dt('No Bower packages to update.'), 'ok');
  }
  else {
    foreach ($operations as $name => $paths) {
      drush_log(dt('Updating Bower dependencies of !name.', array('!name' => $name)), 'notice');

      $command = 'bower update --config.directory=%s --allow-root';
      if (drush_shell_cd_and_exec($paths['cwd'], $command, $paths['lib'])) {
        drush_log(dt('Successfully updated Bower dependencies of !name.', array('!name' => $name)), 'ok');
      }
      else {
        drush_log(dt('Failed to update Bower dependencies of !name.', array('!name' => $name)), 'error');
      }
    }
  }
}

/**
 * Callback function for the aviary-export-hooks command.
 */
function drush_aviary_export_hooks() {
  $operations = array();

  $modules = aviary_list_modules();
  foreach ($modules as $name => $path) {
    $operations[$name] = 'module';
  }

  $themes = aviary_list_themes();
  foreach ($themes as $name => $path) {
    $operations[$name] = 'theme';
  }

  foreach ($operations as $name => $type) {
    if (aviary_export_libraries($type, $name, TRUE)) {
      drush_log(dt('Exported Aviary hooks of !name.', array('!name' => $name)), 'ok');
    }
  }
}

/**
 * Callback function for the aviary-make-generate command.
 */
function drush_aviary_make_generate() {
  $makefile_path = variable_get('aviary_makefile_path', NULL);

  if (empty($makefile_path)) {
    drush_log(dt('Path to makefile is not configured.'), 'error');
  }
  else {
    if (aviary_generate_makefile($makefile_path)) {
      drush_log(dt('Generated makefile !path', array('!path' => $makefile_path)), 'ok');
    }
  }
}
