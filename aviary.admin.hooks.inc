<?php

/**
 * @file
 * Admin page callbacks for the Aviary module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Export form builder.
 */
function aviary_admin_hooks() {
  $form = array();

  $modules = aviary_list_modules();
  $themes = aviary_list_themes();

  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export Bower dependencies to Aviary file'),
    '#description' => t('Builds a <code>hook_libraries_info()</code> implementation for the module or theme, and writes the output to the <code>*.aviary.inc</code> file of the module or theme. Dependencies can be loaded with <code>libraries_load(PACKAGE_NAME)</code>, or attached to elements with <code>[#attached][libraries_load] = array(array(PACKAGE_NAME))</code>.'),
  );

  $options = array();
  foreach ($modules as $name => $path) {
    $export_path = aviary_get_export_path('module', $name);

    if (!file_exists($export_path) || is_writable($export_path)) {
      $options[$name] = $name;
    }
    else {
      $options[$name] = t('!module (not writeable)', array('!module' => $name));
    }
  }

  $form['export']['export_modules'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Modules'),
    '#options' => $options,
    '#default_value' => array_keys($options),
  );

  $options = array();
  foreach ($themes as $name => $path) {
    $export_path = aviary_get_export_path('theme', $name);

    if (!file_exists($export_path) || is_writable($export_path)) {
      $options[$name] = $name;
    }
    else {
      $options[$name] = t('!theme (not writeable)', array('!theme' => $name));
    }
  }

  $form['export']['export_themes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Themes'),
    '#options' => $options,
    '#default_value' => array_keys($options),
  );

  $form['export']['export_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
    '#submit' => array('aviary_admin_hooks_submit'),
  );

  return $form;
}

/**
 * Form submit handler for export form.
 */
function aviary_admin_hooks_submit($form, &$form_state) {
  $operations = array();

  foreach ($form_state['values']['export_modules'] as $key => $value) {
    if (!empty($value)) {
      $operations[] = array('aviary_admin_hooks_batch', array('module', $key));
    }
  }

  foreach ($form_state['values']['export_themes'] as $key => $value) {
    if (!empty($value)) {
      $operations[] = array('aviary_admin_hooks_batch', array('theme', $key));
    }
  }

  if (empty($operations)) {
    drupal_set_message('There are no Bower manifests to export.');
  }
  else {
    $batch = array(
      'title' => 'Exporting Bower manifests',
      'operations' => $operations,
      'finished' => 'aviary_admin_hooks_batch_finished',
      'file' => drupal_get_path('module', 'aviary') . '/aviary.admin.hooks.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param string $type
 * @param string $name
 * @param array $context
 */
function aviary_admin_hooks_batch($type, $name, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  $context['results'][] = aviary_export_libraries($type, $name, TRUE);
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 */
function aviary_admin_hooks_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count manifest processed.', '@count manifests processed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while exporting ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}
