<?php

/**
 * @file
 * Admin page callbacks for the Aviary module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form builder.
 */
function aviary_admin_settings() {
  $form = array();

  $form['aviary_bower_registry_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Bower registry URL'),
    '#default_value' => variable_get('aviary_bower_registry_url', AVIARY_BOWER_REGISTRY_URL),
  );

  return system_settings_form($form);
}
