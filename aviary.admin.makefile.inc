<?php

/**
 * @file
 * Admin page callbacks for the Aviary module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * makefile settings form builder.
 */
function aviary_admin_makefile_settings() {
  $form = array();

  $form['makefile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Makefile settings'),
  );

  $form['makefile']['aviary_makefile_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Absolute path to makefile'),
    '#description' => t('Only required if the generated output should be written to file.'),
    '#default_value' => variable_get('aviary_makefile_path'),
  );

  $form['makefile']['aviary_makefile_write_to_file'] = array(
    '#type' => 'checkbox',
    '#title' => t('Write generated ouput to file. The existing makefile will be overwritten.'),
    '#default_value' => variable_get('aviary_makefile_write_to_file'),
  );

  return system_settings_form($form);
}

/**
 * makefile form builder.
 */
function aviary_admin_makefile_generation() {
  $form = array();

  $write_to_file = variable_get('aviary_makefile_write_to_file', FALSE);
  $makefile_path = variable_get('aviary_makefile_path', NULL);

  if ($write_to_file) {
    if (empty($makefile_path)) {
      drupal_set_message(t('Path to makefile is not configured.'), 'error');
    }
    else {
      if (aviary_generate_makefile($makefile_path)) {
        drupal_set_message(t('Generated makefile.'));
      }
    }
  }
  else {
    $makefile = aviary_generate_makefile(FALSE);
    $form['aviary_makefile'] = array(
      '#type' => 'textarea',
      '#title' => t('Generated makefile'),
      '#description' => t('Copy this into your makefile.'),
      '#readonly' => TRUE,
      '#wysiwyg' => FALSE,
      '#rows' => count(explode("\n", $makefile)) + 1,
      '#default_value' => $makefile,
    );
  }

  return $form;
}
